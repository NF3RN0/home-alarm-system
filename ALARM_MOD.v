`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: N/A
// Engineer: Dustin Martin
//
// Create Date:    16:56:53 01/27/2016
// Design Name:
// Module Name:    ALARM_MOD
// Project Name: HOME_ALARM
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module ALARM_MOD(
    input clk,
    input ARM,
    input DISARM,
    input DOOR_SENSOR,

		//keypad inputs 1 to 9
	 input Kp1,
	 input Kp2,
	 input Kp3,
	 input Kp4,
	 input Kp5,
	 input Kp6,
	 input Kp7,
	 input Kp8,
	 input Kp9,
	 input Kp0,


	 //Outputs
    output reg SPEAKER = 1'b0,
    output reg LED_SYS_ARMED = 1'b0,
    output reg LED_SYS_DISARMED =1'b0
    );

	 //STATE ASSGINMENTS

`define zero 2'b00 //DISARM STATE
`define one 2'b01		//ARM STATE
`define two 2'b10		//ALARM STATE
`define three 2'b11	//Combination checker






//REGS
reg [1:0] state, next_state;

reg [4:0] in_reg; //input reg

reg [32:0] Keypad_reg; //

reg [32:0] Combo_reg; // Combination to check: 0001 0010 0011 0100



always @(posedge clk)

	if (DISARM) state= `zero;
	else			state= next_state;

always @ (ARM or DOOR_SENSOR or state or DISARM)
	case (state)
		`zero:
		begin

		//DISARM = 0'b0;
		//syntax for zero

			SPEAKER =1'b0; //MAKE SURE THE SPEAKER IS NOT ON

			LED_SYS_DISARMED =1'b1; //NOTIFY USER THAT SYSTEM IS NOT ARMED
			LED_SYS_ARMED = 1'b0;
			if (ARM == 1'b1) next_state = `one;
			else		next_state = `zero;
		end
		`one:
		begin
			//syntax for one
				LED_SYS_DISARMED =1'b0; //NOTIFY USER THAT SYSTEM IS ARMED
				LED_SYS_ARMED = 1'b1;
				SPEAKER =1'b0; //MAKE SURE THE SPEAKER IS NOT ON

				if (DISARM) next_state =`three; //CHECK TO SEE IF THE USER WANTS TO DISARM if they do check the combination
					else
					begin
				if (DOOR_SENSOR == 1'b1 && ARM== 1'b1) next_state =`two; //CHECK TO SEE IF THE DOOR IS OPEN
				else					next_state =`one;
					end
		end
		`two:
		begin

		//syntax for state two

					SPEAKER = 1'b1; //NOTIFY THE USER THAT THE DOOR IS OPEN, SOUND THE ALARM

				if (DISARM == 1'b1) next_state =`one;
				else		   next_state =`two;
		end

		//check combination
		`three:
		begin

			//read in the input

				//check the input to the combination
			if(Combo_reg == Keypad_reg) next_state =`zero;

			else next_state=`two; // combination is wrong and we sound the alarm


		end

		default: next_state =`zero;
	endcase




endmodule
