`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:   18:30:09 01/27/2016
// Design Name:   ALARM_MOD
// Module Name:
// Project Name:  HOME_ALARM
// Target Device:
// Tool versions:
// Description:
//
// Verilog Test Fixture created by ISE for module: ALARM_MOD
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////

module ALARM_TEST;

	// Inputs
	reg clk;
	reg ARM;
	reg DISARM;
	reg DOOR_SENSOR;

	// Outputs
	wire SPEAKER;
	wire LED_SYS_ARMED;
	wire LED_SYS_DISARMED;

	integer i = 0;
	parameter numinputs = 2;
	parameter maxcount = (1 << numinputs);


	// Instantiate the Unit Under Test (UUT)
	ALARM_MOD uut (
		.clk(clk),
		.ARM(ARM),
		.DISARM(DISARM),
		.DOOR_SENSOR(DOOR_SENSOR),
		.SPEAKER(SPEAKER),
		.LED_SYS_ARMED(LED_SYS_ARMED),
		.LED_SYS_DISARMED(LED_SYS_DISARMED)
	);


	always

	#10 clk = !clk;



	initial begin
		// Initialize Inputs
		clk = 0;
		ARM = 0;
		DISARM = 0;
		DOOR_SENSOR = 0;

		// Wait 100 ns for global reset to finish
		#100;



		for (i=0; i <=maxcount; i=i+1); begin

			{ARM,DOOR_SENSOR,DISARM} = i;  //for loop test all inputs for different values NOTICE!!! Tested values were changed based on what needed to be tested

			#100;
			end

		// Add stimulus here

	end

endmodule
